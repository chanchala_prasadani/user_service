package com.userservice.User.Service.controllers;

import com.userservice.User.Service.dtos.OrderDTO;
import com.userservice.User.Service.dtos.UserDTO;
import com.userservice.User.Service.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/user")
public class UserController {

    @Autowired
    private UserService userService;

    //Localhost:8088/api/user/getAll
    @GetMapping("/getAll")
    public List<UserDTO> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId(@PathVariable final Long id) {return userService.getOrdersByUserId(id);}

    @PostMapping("/save")
    public boolean authenticateUserByPost (@RequestBody Map<String,String> data){
        UserDTO user = new UserDTO(data.get("name"),data.get("age"));
        return userService.saveUser(user);
    }
}
