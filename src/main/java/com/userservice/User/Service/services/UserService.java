package com.userservice.User.Service.services;

import com.userservice.User.Service.dtos.OrderDTO;
import com.userservice.User.Service.dtos.UserDTO;
import com.userservice.User.Service.entities.UserEntity;
import com.userservice.User.Service.repositories.UserRepository;
import com.userservice.User.Service.util.validations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Value("${order-service.base-url}")
    private String orderServiceBaseUrl;

    @Value("${order-service.order-url}")
    private String orderServiceOrderUrl;

    @Autowired
    private UserRepository repository;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    public List<UserDTO> getAllUsers(){
        List<UserDTO> users = null;
        LOGGER.info("================Entered getAllUsers() ==================");
        try {
            users = repository.findAll().stream().map(
                    userEntity -> new UserDTO(
                            userEntity.getId().toString(),
                            userEntity.getName(),
                            userEntity.getAge()
                    )
            ).collect(Collectors.toList());
        }catch (Exception e){
            System.out.println("Error------------------------");
            e.printStackTrace();
        }

        return users;
    }

    public List<OrderDTO> getOrdersByUserId(Long id){
        List<OrderDTO> orders = null;
        try {
            orders = restTemplateBuilder.build().getForObject(orderServiceBaseUrl.concat(orderServiceOrderUrl).concat("/"+id),List.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        return orders;
    }

    //This method is used to save users
    public boolean saveUser(UserDTO user){
        LOGGER.info("==========In the save method============");
        try{
            if(validations.validateUser(user)){
                UserEntity userEntity = new UserEntity(user.getName(),user.getAge());
                repository.save(userEntity);
                return true;
            }else{
                LOGGER.info("=========Validation Failed==========");
                return false;
            }
        }catch (Exception e){
            LOGGER.info("===========ERROR in User Save============"+e.getMessage());
            return false;
        }
    }
}