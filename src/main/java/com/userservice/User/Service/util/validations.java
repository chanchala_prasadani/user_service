package com.userservice.User.Service.util;

import com.userservice.User.Service.dtos.UserDTO;

public class validations {
    public static boolean validateUser(UserDTO user){
        if(user.getName()==null || user.getAge()==null){
            return false;
        }else if (Integer.parseInt(user.getAge())>120 || Integer.parseInt(user.getAge())<0){
            return false;
        }else{
            return true;
        }
    }
}
